function Calculator( element ) {
	this.element = element;
	this.$element = $(element);
	this.init();
}

Calculator.prototype.init = function(){
	this.inputDiameterOrRadius = 0;
	this.inputResultOne = 0;
	this.inputResultFull = 0;
	this.pressValue = 0;
	this.inputSetCord = 0;
	this.$diameterOrRadius = this.$element.find('.diameterOrRadius');
	this.$setCord = this.$element.find('#setCord');
	this.$Result = this.$element.find('.result');
	this.$element.find('#pressCalculate').on('click', {parent:this}, this.pressCalculateResult);
	this.$element.find('#pressReset').on('click', {parent:this}, this.pressReset);
	this.$element.find('#addResistor').on('click', {parent:this}, this.pressAddResistor);
	this.$element.find('#delResistor').on('click', {parent:this}, this.pressDelResistor);
	this.$element.find('#resPressCalculate').on('click', {parent:this}, this.resPressCalculateResult);
	this.$element.find('#resPressReset').on('click', {parent:this}, this.resPressReset);
	
	this.$element.find('#addCapactitor').on('click', {parent:this}, this.pressAddCapactitor);
	this.$element.find('#delCapactitor').on('click', {parent:this}, this.pressDelCapactitor);
	this.$element.find('#capPressCalculate').on('click', {parent:this}, this.capPressCalculateResult);
	this.$element.find('#capPressReset').on('click', {parent:this}, this.capPressReset);
};

Calculator.prototype.pressCalculateResult = function(event){
	var that = event.data.parent;
	if (that.$element.find(".selectDiameter").prop("checked")){
		that.inputDiameterOrRadius = that.$diameterOrRadius.val();
	} else {
		that.inputDiameterOrRadius = 2*that.$diameterOrRadius.val();
	}
	that.inputSetCord = that.$setCord.val();
	that.inputResultOne = 3.14*that.inputDiameterOrRadius*that.inputDiameterOrRadius/4;
	that.$element.find('#area').text(that.inputResultOne.toFixed(2));
	that.inputResultFull = that.inputResultOne*that.inputSetCord;
	that.$element.find('#areaFull').text(that.inputResultFull.toFixed(2));
	if (that.$diameterOrRadius.val() == 0){
		alert("Пожалуйста введите значение: диаметр или радиус")
	}
};

Calculator.prototype.pressReset = function(event){
	var that = event.data.parent;
	that.$element.find('#area').text("");
	that.$element.find('#areaFull').text("");
	that.$element.find('.diameterOrRadius').val("");
	that.$element.find('#setCord').val("1");
};

Calculator.prototype.pressAddResistor = function(event){
	var that = event.data.parent;
	var resBlock = that.$element.find('.resAllInputBlock');
	var template = that.$element.find('.resAllInputBlock > div:first').html();
	if(that.$element.find('.resAllInputBlock > div').length < 6){
		resBlock.append(template);
	}
};

Calculator.prototype.pressDelResistor = function(event){
	var that = event.data.parent;
	if (that.$element.find('.resAllInputBlock > div').length > 2){
		that.$element.find('.resAllInputBlock > div:last').remove();
	}
};

Calculator.prototype.resPressCalculateResult = function(event){
	var that = event.data.parent;
	var arrInputValue = [];
	var summ = 0;
	var koef = 0;
	if (that.$element.find(".selectPosled").prop("checked") && (that.$element.find('.resAllInputBlock input').val() != "")) {
		that.$element.find('.resAllInputBlock input').each(function(){
			summ += parseFloat($(this).val());
		});
		that.$element.find('#resAreaFull').text("R="+summ+"Om");
	} else if(that.$element.find(".selectParalel").prop("checked")) {
		that.$element.find('.resAllInputBlock input').each(function(){
			summ += 1/parseFloat($(this).val());
		});
		that.$element.find('#resAreaFull').text("R="+(1/summ).toFixed(4)+"Om");
	} else {
		alert("Не выбран тип соединения резисторов или сопротивление резисторов!")
	}
};

Calculator.prototype.resPressReset = function(event){
	var that = event.data.parent;
	that.$element.find('#resAreaFull').text("");
	that.$element.find('.selectPosled').removeAttr("checked");
	that.$element.find('.selectParalel').removeAttr("checked");
	that.$element.find('.resAllInputBlock input').val("");
	that.$element.find('.resAllInputBlock input').each(function(){
		if(that.$element.find('.resAllInputBlock input').length > 2){
			that.$element.find('.resAllInputBlock > div:last').remove();
		}
	});
};

/* Соединение конденсаторов  */


Calculator.prototype.pressAddCapactitor = function(event){
	var that = event.data.parent;
	var capBlock = that.$element.find('.capAllInputBlock');
	var capTemplate = that.$element.find('.capAllInputBlock > div:first').html();
	if(that.$element.find('.capAllInputBlock > div').length < 6){
		capBlock.append(capTemplate);
	}
};

Calculator.prototype.pressDelCapactitor = function(event){
	var that = event.data.parent;
	if (that.$element.find('.capAllInputBlock > div').length > 2){
		that.$element.find('.capAllInputBlock > div:last').remove();
	}
};

Calculator.prototype.capPressCalculateResult = function(event){
	var that = event.data.parent;
	var arrInputValue = [];
	var summ = 0;
	var koef = 0;
	if (that.$element.find(".capSelectParalel").prop("checked") && (that.$element.find('.capAllInputBlock input').each(function(){$(this).val() !==""}) )) {
		that.$element.find('.capAllInputBlock input').each(function(){
			summ += parseFloat($(this).val());
		});
		that.$element.find('#capAreaFull').text("R="+summ+"Om");
	} else if(that.$element.find(".capSelectPosled").prop("checked")) {
		that.$element.find('.capAllInputBlock input').each(function(){
			summ += 1/parseFloat($(this).val());
		});
		that.$element.find('#capAreaFull').text("R="+(1/summ).toFixed(4)+"Om");
	} else {
		alert("Не выбран тип соединения конденсаторов или емкость конденсаторов!")
	}
};

Calculator.prototype.capPressReset = function(event){
	var that = event.data.parent;
	that.$element.find('#capAreaFull').text("");
	that.$element.find('.capSelectPosled').removeAttr("checked");
	that.$element.find('.capSelectParalel').removeAttr("checked");
	that.$element.find('.capAllInputBlock input').val("");
	that.$element.find('.capAllInputBlock input').each(function(){
		if(that.$element.find('.capAllInputBlock input').length > 2){
			that.$element.find('.capAllInputBlock > div:last').remove();
		}
	});
};


var firstCalc = document.getElementsByClassName('calculusElectronic')[0];
var calc = new Calculator(firstCalc);



